<?php

namespace Drupal\export_tools;

/**
 * Defines the export tools exception class.
 */
class ExportToolsException extends \Exception {
}
