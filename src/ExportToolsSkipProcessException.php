<?php

namespace Drupal\export_tools;

/**
 * Defines the export tools skip process exception class.
 */
class ExportToolsSkipProcessException extends \Exception {
}
