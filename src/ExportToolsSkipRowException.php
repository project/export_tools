<?php

namespace Drupal\export_tools;

/**
 * Defines the export tools skip row exception class.
 */
class ExportToolsSkipRowException extends \Exception {
}
