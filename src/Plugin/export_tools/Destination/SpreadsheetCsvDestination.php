<?php

namespace Drupal\export_tools\Plugin\export_tools\Destination;

/**
 * Provides SpreadsheetCsv destination plugin.
 *
 * @ExportDestination(
 *   id = "spreadsheet.csv"
 * )
 */
class SpreadsheetCsvDestination extends SpreadsheetBaseDestination {}
