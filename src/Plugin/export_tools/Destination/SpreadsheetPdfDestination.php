<?php

namespace Drupal\export_tools\Plugin\export_tools\Destination;

/**
 * Provides Spreadsheet PDF destination plugin.
 *
 * @ExportDestination(
 *   id = "spreadsheet.pdf"
 * )
 */
class SpreadsheetPdfDestination extends SpreadsheetBaseDestination {

  /**
   * The spreadsheet extension to save.
   *
   * @var string
   */
  protected $extension = 'pdf';

}
